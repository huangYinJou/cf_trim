# -*- coding: utf-8 -*-
import re
import sys
import argparse
from sqlalchemy import *
from utils import removeHira, get_url

def write_rep_table(input_file, cursor):
    with open(options.input_file, 'r') as fn:
        for line in fn:
            row_data = convert_rep_line(line)
            cursor.execute(**row_data)

def convert_rep_line(line):
    line = line.rstrip().decode('utf-8')
    cf_id, cf_str = line.split("##")
    row_data = {"cf_id": cf_id, "rep_str": cf_str}
    return row_data

def write_cfsim_table(input_file, cursor):
    with open(options.input_file, 'r') as fn:
        for line in fn:
            row_data = convert_cfsim_line(line)
            if row_data:
                cursor.execute(**row_data)

def convert_cfsim_line(line):
    line = line.rstrip().decode('utf-8')
    pred, pred_num, sim_cf_nums, sim_scores = line.rstrip().split("##")

    # trim bad predicate.
    # depricate?
    pred, pred_type = pred.split(':')
    if '~' in pred:
        return

    row_data = {"pred": removeHira(pred), "pred_num": pred_num, "sim_cf_nums": sim_cf_nums, "sim_scores": sim_scores}

    pred_str = "%s:%s" % (pred, pred_type)
    row_data["pred_complete"] = pred_str
    row_data["url"] = get_url(pred_str)

    return row_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file", action="store", dest="input_file")
    parser.add_argument("-o", "--output_db", action="store", dest="output_db")

    parser.add_argument("--write_rep_str", action="store_true", dest="write_rep_str")
    parser.add_argument("--write_cf_sim", action="store_true", dest="write_cf_sim")
    parser.add_argument("--table_name", action="store", dest="table_name")
    options = parser.parse_args()

    engine = create_engine('sqlite:///%s' % options.output_db, echo=False)
    metadata = MetaData(engine)

    if options.write_rep_str:
        output_table = Table('rep', metadata, 
                             Column("cf_id", String()),
                             Column("rep_str", String())
                            )
        output_table.create()
        insert_cursor = output_table.insert()

        write_rep_table(options.input_file, insert_cursor)

    elif options.write_cf_sim:
        output_table = Table(options.table_name, metadata, 
                             Column("pred", String()), 
                             Column("pred_num", Integer()), 
                             Column("sim_cf_nums", String()), 
                             Column("sim_scores", String()), 
                             Column("url", String()), 
                             Column("pred_complete", String())
                            )

        output_table.create()
        insert_cursor = output_table.insert()

        write_cfsim_table(options.input_file, insert_cursor)

