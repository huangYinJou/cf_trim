# -*- coding: utf-8 -*-
import sys
import re
import operator
from math import sqrt
import xml.etree.ElementTree as ET
import utils
from utils import CASE_ENG, CASE_KATA, KATA_ENG, ENG_HIRA
from CDB_Reader import CDB_Reader

class CaseFrame(object):
    id_pattern = r"(\D+):(\D+)(\d+)$"
    def __init__(self, cf_cdb, cf_id, core_ratio=0.05):
        self._set_predicate(cf_id)

        self.CF_CDB = CDB_Reader(cf_cdb)
        self._set_args()

        self.core_ratio = core_ratio

    def _set_predicate(self, cf_id):
        self.id = cf_id
        self.predRep, self.predType, self.predNum = re.search(CaseFrame.id_pattern, cf_id).groups()

    def _set_args(self):
        cf_xml = self._get_xml()
        if cf_xml == None:
            return

        self.caseFrequencies, self.args = {}, {}
        for case_xml in cf_xml:
            case_kata, freq = case_xml.attrib['case'], case_xml.attrib['frequency']

            if case_kata not in CASE_KATA:
                continue
            case = KATA_ENG[case_kata]

            args_dict = { arg.text.encode('utf-8'): int(arg.attrib['frequency']) for arg in case_xml if 'sm' not in arg.attrib.keys() }
            if args_dict != {}:
                self.caseFrequencies[case] = int(freq)
                self.args[case] = args_dict

        # check for empty CF.
        if self.args == {}:
            sys.stderr.write("Empty case frame: %s\n" % self.id)

    def _get_xml(self):
        cf_xml = self.CF_CDB.get(self.id)
        if cf_xml:
            return ET.fromstring(cf_xml)
        else:
            sys.stderr.write("Cannot find case frame with id %s.\n" % (self.id))
            return None

    def __str__(self):
        return self._get_rep_str()

    def _get_rep_str(self):
        rep_str = ""

        core_cases = self._get_core_cases()
        for case in core_cases:
            case_args = self.args[case]
            dominant_arg = max(case_args, key=case_args.get)

            rep_str += "%s %s " % (dominant_arg, ENG_HIRA[case])

        rep_str += self.predRep

        return rep_str

    def _get_core_cases(self):
        if self.caseFrequencies == {}:
            return []

        threshold = self.core_ratio * max(self.caseFrequencies.values())
        core_cases = [case for case, freq in self.caseFrequencies.iteritems() if freq > threshold]
        return core_cases


class CaseFrameGroup(object):
    pred_pattern = r"(\D+):(\D+)(\d*)$"
    def __init__(self, cf_cdb, predStr):
        self._set_predicate(predStr)
        self._set_cfs(cf_cdb)
        self._check_valid_predNum()

    def _set_predicate(self, predStr):
        try:
            self.pred, self.predType, predNum = re.search(CaseFrameGroup.pred_pattern, predStr).groups()
        except:
            sys.stderr.write("Input predicate string not valid: %s\n" % predStr)
            sys.exit(1)

        self.predNum = predNum if predNum != "" else None
            
    def _set_cfs(self, cf_cdb):
        self._set_id_list(cf_cdb)
        if self.cf_ids == None:
            sys.stderr.write("Input predicate not in repository: %s:%s\n" % (self.pred, self.predType))
            sys.exit(1)

        self.cfs_with_wo, self.cfs_no_wo = [], []
        for cf_id in self.cf_ids:
            cf = CaseFrame(cf_cdb, cf_id)

            if 'w' in cf.args.keys():
                self.cfs_with_wo.append(cf)
            else:
                self.cfs_no_wo.append(cf)

    def _set_id_list(self, cf_cdb):
        CF_CDB = CDB_Reader(cf_cdb)

        predCounts = CF_CDB.get(self.pred)
        if predCounts == None or self.predType not in predCounts:
            self.cf_ids = None
            return
        
        predCountDict = dict(x.split(':') for x in predCounts.split('/'))
        cf_count = int(predCountDict[self.predType])
        self.cf_ids = [ "%s:%s%s" % (self.pred, self.predType, index) for index in xrange(1, cf_count + 1) ]

    def _check_valid_predNum(self):
        """
        check if predNum is valid. (i.e. if the input cf is without ヲ-case.)
        """
        if self.predNum == None:
            return

        nums_no_wo = [cf.predNum for cf in self.cfs_no_wo]
        if self.predNum not in nums_no_wo:
            sys.stderr.write("Please specipy a cf-id without wo-case:\n")
            sys.stderr.write( "%s\n" % "\n".join(cf.id for cf in self.cfs_no_wo) )
            sys.exit(1)

    def export_rep_strs(self):
        for cf in self.cfs_with_wo + self.cfs_no_wo:
            yield cf.id, str(cf)

    def export_all_sim_profile(self, without_alignment=False, inclusive_cosim=False, upper_limit=None, debug=False):
        for cf_no in self.cfs_no_wo:
            if self.predNum != None and cf_no.predNum != self.predNum:
                continue

            cf_sim_scores = self.get_sim_score_for_cf(cf_no, without_alignment, inclusive_cosim, upper_limit)
            if cf_sim_scores == []:
                continue

            if debug:
                print "%s (%s)" % (cf_no.id, cf_no)
                for cf, sim_score in cf_sim_scores:
                    print "\t%s (%s) %s" % (cf.id, cf, sim_score)

            else:
                pred = "%s:%s" % (self.pred, self.predType)
                sim_cfs, sim_scores = zip(*cf_sim_scores)
                yield pred, cf_no.predNum, utils.encode_list(cf.predNum for cf in sim_cfs), utils.encode_list(sim_scores) 

    def get_sim_score_for_cf(self, cf_no, without_alignment, inclusive_cosim, upper_limit):
        cf_sim_scores = []
        for cf_with in self.cfs_with_wo:
            sim_score = self.get_cf_sim(cf_no, cf_with, without_alignment, inclusive_cosim)
            cf_sim_scores.append( (cf_with, sim_score) )

        cf_sim_scores = sorted(cf_sim_scores, key=lambda x: x[1], reverse=True)[:upper_limit]
        return cf_sim_scores


    def get_cf_sim(self, cf_no, cf_with, without_alignment=False, inclusive_cosim=False):
        weightedCaseSim = self._get_weighted_case_sim(cf_no, cf_with, inclusive_cosim)
        cfAlignSim = self._get_cf_alignment_sim(cf_no, cf_with) if not without_alignment else 1

        return round(weightedCaseSim * cfAlignSim, 3)

    def _get_weighted_case_sim(self, cf_no, cf_with, inclusive_cosim=False):
        args_no, args_with = cf_no.args, cf_with.args
        freq_no, freq_with = cf_no.caseFrequencies, cf_with.caseFrequencies

        weighted_case_sim, total_weight = 0, 0
        for case in set(args_no.keys()) & set(args_with.keys()):
            if inclusive_cosim:
                case_args_with = { word: count for word, count in args_with[case].iteritems() if word in args_no[case] }
            else:
                case_args_with = args_with[case]

            case_sim = utils.cosineSimilarity(args_no[case], case_args_with)
            weight = sqrt(freq_no[case] * freq_with[case])
            weighted_case_sim += case_sim * weight

            total_weight += weight

        if total_weight == 0:
            return 0
        return weighted_case_sim / total_weight

    def _get_cf_alignment_sim(self, cf1, cf2, neglect_wo=True):
        freq1, freq2 = cf1.caseFrequencies, cf2.caseFrequencies
        total_count1, total_count2 = sum(freq1.values()), sum(freq2.values())
        if neglect_wo:
            total_count2 -= freq2['w']

        common_count1, common_count2 = 0, 0
        for case in set(freq1.keys()) & set(freq2.keys()):
            common_count1 += freq1[case]
            common_count2 += freq2[case]

        if total_count1 == 0 or total_count2 == 0:
            return 0

        align_score = float(common_count1 * common_count2) / (total_count1 * total_count2)
        return sqrt(align_score)

