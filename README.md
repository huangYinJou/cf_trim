# cf_trim
## Basic Usage
1. Query a case frame without ヲ-case for its similarities with other case frames with ヲ-case:
  ```
  python query_predicate.py --cdb_keymap CF_CDB --pred PRED --plain_cosim --cf_num_limit 5
  ```

2. Export the similarity profiles of all the predicates to a sqlite database.
  ```
  ./ExtractCfSim.sh OUTPUT_DIR --cdb_keymap CF_CDB --plain_cosim --cf_num_limit 5
  ```

3. Options:

  - For **CF_CDB**, use one of the following cf database:

    - The latest version of case frames:
        ```
        /windroot/huang/cf_cdbs/20170502/new_cdb/new_cf.cdb.keymap
        ```

    - The latest version of case frames, but only the less problematic ones (90% cut):
        ```
        /windroot/huang/cf_cdbs/20170502_good/new_cdb/new_cf.cdb.keymap
        ```

    - An older version of case frames:
        ```
        /windroot/huang/cf_cdbs/20160205/new_cdb/new_cf.cdb.keymap
        ```

  - For **PRED**, use one of the following forms:
    - A case frame without ヲ-case. (ex: 食べる/たべる:動7)
    - A predicate without case number, and the similarity profile of all the case frames without ヲ-case will be printed. (ex: 食べる/たべる:動)

  - There are 4 types of **similarity measures**, apply one or more of them: 
    - `--plain_cosim`
    - `--inclusive_cosim`
    - `--plain_without_alignment`
    - `--inclusive_without_alignment`

  - To print top N most similar case frames with PRED, use:
    ```
    --cf_num_limit n
    ```

    (specifying nothing will print all.)

  - Specify arbitrary **OUTPUT_DIR**, the database along with the log file will be exported here.
