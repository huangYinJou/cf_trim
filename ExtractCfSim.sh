#!/bin/bash


## parse command-line arguments.

# mandatory arguments:
output_dir="$1"
mkdir -p $output_dir
rm -f $output_dir/*
shift

#optional arguments:
extract_postfix=""
for i in "$@"
do
    case $1 in
        "--plain_cosim")
            plain_output_file=$output_dir/plain_cosim.tmp
            extract_postfix=$extract_postfix"--plain_cosim $plain_output_file "
            shift
            ;;
        "--inclusive_cosim")
            inclusive_output_file=$output_dir/inclusive_cosim.tmp
            extract_postfix=$extract_postfix"--inclusive_cosim $inclusive_output_file "
            shift
            ;;
        "--plain_without_alignment")
            without_align_output_file=$output_dir/plain_without_alignment.tmp
            extract_postfix=$extract_postfix"--plain_without_alignment $without_align_output_file "
            shift
            ;;
        "--inclusive_without_alignment")
            without_align_output_file=$output_dir/inclusive_without_alignment.tmp
            extract_postfix=$extract_postfix"--inclusive_without_alignment $without_align_output_file "
            shift
            ;;
        "--cdb_keymap")
            cdb_keymap="$2"
            shift
            shift
            ;;
        "--pred_list_file")
            pred_list_file="$2"
            shift
            shift
            ;;
    esac
done

# optional arguments handling.
cdb_keymap_default=/windroot/huang/cf_cdbs/20170502_good/new_cdb/new_cf.cdb.keymap
cdb_keymap="${cdb_keymap:-$cdb_keymap_default}"

pred_list_file_default=/windroot/huang/build_cf_sim_db/pred_types.txt
pred_list_file="${pred_list_file:-$pred_list_file_default}"


## load scripts.
project_dir=`dirname $0`
extract_script=$project_dir/extract_cf_sim.py
build_script=$project_dir/build_cf_sim_db.py

## extract similaity scores of each predicate.

# write to intermediate files.

rep_file=$output_dir/cf_rep.tmp
log_file=$output_dir/log.txt

N=` wc -l "$pred_list_file" | cut -f1 -d' ' `
percent=$(( N / 100 ))
n=0

echo "extract cf similarity scores from: $cdb_keymap"
echo -ne "\t["
while read line; do
    python $extract_script --cdb_keymap $cdb_keymap --pred $line --cf_rep $rep_file $extract_postfix 2>> $log_file

    n=$((n+1))
    if (( $n % $percent == 0 ))
    then
        if (( $n % ($percent * 10) == 0 ))
        then
            echo -n "*"
        else
            echo -n "="
        fi
    fi
done < $pred_list_file
echo -ne "]\n"
echo "done."


# write to db.
cf_sim_db=$output_dir/cf_sim.sqlite
echo "write result to sqlite database: $cf_sim_db"

for f in $output_dir/*.tmp
do
    base=$(basename $f)
    table_name="${base%.*}"
    case $table_name in
        "cf_rep" )
            echo -e "\twriting table: $table_name"
            python $build_script -i $f -o $cf_sim_db --write_rep_str
            ;;

        "plain_cosim"|"inclusive_cosim"|"plain_without_alignment"|"inclusive_without_alignment" )
            echo -e "\twriting table: $table_name"
            python $build_script -i $f -o $cf_sim_db --write_cf_sim --table_name $table_name
            ;;

        * )
            echo "skiping suspicious file: $f"
            ;;
    esac
done

echo "done."


## clean-up.
while true; do
    echo "Intermediate files: "
    for f in $output_dir/*.tmp; do echo -e "\t$f"; done

    read -p "Delete them?([y/n])  " yn
    case $yn in
        [Yy]* )
            rm -vf $output_dir/*.tmp
            break
            ;;

        [Nn]* )
            exit
            ;;

        * )
            echo "Please answer yes/no."
            ;;
    esac
done

