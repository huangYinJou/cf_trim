# -*- coding: utf-8 -*-
import re
import sys
from math import sqrt
import urllib

CASE_ENG = ['g', 'w', 'n', 'd', 'to', 'kara', 'yori', 'he', 'made']
CASE_KATA = [u"ガ格", u"ヲ格", u"ニ格", u"デ格", u"ト格", u"カラ格", u"ヨリ格", u"ヘ格", u"マデ格"]
CASE_HIRA = [u"が", u"を", u"に", u"で", u"と", u"から", u"より", u"へ", u"まで"]

ENG_HIRA = dict(zip(CASE_ENG, CASE_HIRA))
ENG_KATA = dict(zip(CASE_ENG, CASE_KATA))
KATA_ENG = dict(zip(CASE_KATA, CASE_ENG))

def removeHira(verboseStr, splitChar=['+'], concatenate=False):
    verboseStrs = re.split(r'[%s]+' % ("".join(splitChar)), verboseStr)
    shortStrs = map(lambda x: x.split('/')[0], verboseStrs)

    if concatenate:
        return "".join(shortStrs)
    return "+".join(shortStrs)

def cosineSimilarity(v1, v2, strip=False, skip_set = [u"<主体準>"], restrict_set=[], get_contributors=False, threshold=0):
    """
    calculate cosine similarity of two dictionary-vector.
    """
    norm_v1, norm_v2 = vector_norm(v1), vector_norm(v2)
    denom = norm_v1 * norm_v2
    if denom == 0:
        return 0 if not get_contributors else (0, [])

    if strip:
        v1 = {"+".join(map(lambda x: x.split('/')[0], arg.split('+'))) : count for arg, count in v1.iteritems()}
        v2 = {"+".join(map(lambda x: x.split('/')[0], arg.split('+'))) : count for arg, count in v2.iteritems()}

    if threshold != 0:
        restrict_set1 = [arg for arg, count in v1.iteritems() if count > threshold]
        restrict_set2 = [arg for arg, count in v2.iteritems() if count > threshold]
        restrict_set = set(restrict_set1) & set(restrict_set2)

    # calculate inner product
    inner, contribute_list = 0, []
    for w in set(v1.keys()).intersection(set(v2.keys())):
        if w in skip_set:
            continue
        if restrict_set == [] or w in restrict_set:
            inner += v1[w]*v2[w]
            if get_contributors:
                contribute_list.append("%s(%s,%s)" % (w, v1[w],v2[w]))

    return inner/denom if not get_contributors else (inner/denom, "<br>".join(contribute_list) )

def vector_norm(v):
    n2 = 0
    for key, value in v.iteritems():
        if key == 'all':
            continue
        n2 += value ** 2
    return sqrt(n2)

def get_url(id_str):
    return urllib.quote_plus(id_str.encode('utf-8'))

def encode_list(L):
    """
    ex: ['a', 'b', 'c'] => "0=a&1=b&2=c"
    """
    return "&".join(["%s=%s" % (index, element) for index, element in enumerate(L)])

def encode_dict(d):
    """
    ex: {'k1' : 'v1', 'k2' : 'v2'} => "k1=v1&k2=v2"
    """
    return "&".join(["%s=%s" % (align, score) for align, score in d.items() if '_' not in align])

