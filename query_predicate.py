# -*- coding: utf-8 -*-
import sys
import argparse
from CaseFrame import CaseFrame, CaseFrameGroup

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cdb_keymap", dest="cdb_keymap")
    parser.add_argument("-p", "--pred", dest="pred")
    parser.add_argument("-l", "--cf_num_limit", type=int, dest="cf_num_limit")

    parser.add_argument("--plain_cosim", action="store_true", dest="plain_cosim")
    parser.add_argument("--inclusive_cosim", action="store_true", dest="inclusive_cosim")
    parser.add_argument("--plain_without_alignment", action="store_true", dest="plain_without_alignment")
    parser.add_argument("--inclusive_without_alignment", action="store_true", dest="inclusive_without_alignment")

    options = parser.parse_args()

    cfg = CaseFrameGroup(options.cdb_keymap, options.pred)

    if options.plain_cosim:
        print "<plain cosim>"
        for result in cfg.export_all_sim_profile(inclusive_cosim=False, without_alignment=False, debug=True, upper_limit=options.cf_num_limit):
            pass

    if options.inclusive_cosim:
        print "<inclusive cosim>"
        for result in cfg.export_all_sim_profile(inclusive_cosim=True, without_alignment=False, debug=True, upper_limit=options.cf_num_limit):
            pass

    if options.plain_without_alignment:
        print "<inclusive cosim without alignment>"
        for result in cfg.export_all_sim_profile(inclusive_cosim=False, without_alignment=True, debug=True, upper_limit=options.cf_num_limit):
            pass

    if options.inclusive_without_alignment:
        print "<inclusive cosim without alignment>"
        for result in cfg.export_all_sim_profile(inclusive_cosim=True, without_alignment=True, debug=True, upper_limit=options.cf_num_limit):
            pass

