# -*- coding: utf-8 -*-
import sys
import argparse
from CaseFrame import CaseFrame, CaseFrameGroup

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--cdb_keymap", dest="cdb_keymap")
    parser.add_argument("-p", "--pred", dest="pred")
    parser.add_argument("-l", "--cf_num_limit", type=int, dest="cf_num_limit")

    parser.add_argument("--plain_cosim", action="store", dest="plain_cosim")
    parser.add_argument("--inclusive_cosim", action="store", dest="inclusive_cosim")
    parser.add_argument("--plain_without_alignment", action="store", dest="plain_without_alignment")
    parser.add_argument("--inclusive_without_alignment", action="store", dest="inclusive_without_alignment")
    parser.add_argument("--cf_rep", action="store", dest="cf_rep")

    options = parser.parse_args()

    cfg = CaseFrameGroup(options.cdb_keymap, options.pred)
    if options.plain_cosim:
        with open(options.plain_cosim, 'a') as f:
            for line in cfg.export_all_sim_profile(inclusive_cosim=False, without_alignment=False):
                f.write("##".join(line) + '\n')

    if options.inclusive_cosim:
        with open(options.inclusive_cosim, 'a') as f:
            for line in cfg.export_all_sim_profile(inclusive_cosim=True, without_alignment=False):
                f.write("##".join(line) + '\n')

    if options.plain_without_alignment:
        with open(options.plain_without_alignment, 'a') as f:
            for line in cfg.export_all_sim_profile(inclusive_cosim=False, without_alignment=True):
                f.write("##".join(line) + '\n')

    if options.inclusive_without_alignment:
        with open(options.inclusive_without_alignment, 'a') as f:
            for line in cfg.export_all_sim_profile(inclusive_cosim=True, without_alignment=True):
                f.write("##".join(line) + '\n')

    if options.cf_rep:
        with open(options.cf_rep, 'a') as f:
            for line in cfg.export_rep_strs():
                f.write("##".join(line) + '\n')

